package com.sn.mylinelogin;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.linecorp.linesdk.LoginDelegate;
import com.linecorp.linesdk.LoginListener;
import com.linecorp.linesdk.Scope;
import com.linecorp.linesdk.auth.LineAuthenticationParams;
import com.linecorp.linesdk.auth.LineLoginResult;
import com.linecorp.linesdk.widget.LoginButton;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private LoginDelegate loginDelegate = LoginDelegate.Factory.create();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LoginButton loginButton = findViewById(R.id.line_login_btn);
        loginButton.setChannelId("1653755565");
        loginButton.enableLineAppAuthentication(true);
        loginButton.setAuthenticationParams(new LineAuthenticationParams.Builder()
                .scopes(Arrays.asList(Scope.PROFILE))
                .build()
        );
        loginButton.setLoginDelegate(loginDelegate);
        loginButton.addLoginListener(new LoginListener() {
            @Override
            public void onLoginSuccess(@NonNull LineLoginResult result) {
                Log.d("LINE RESULT",result.getLineProfile()+"");
                Toast.makeText(MainActivity.this, "Login success"+result.getLineProfile(),
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLoginFailure(@Nullable LineLoginResult result) {
                Toast.makeText(MainActivity.this, "Login failure",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}
